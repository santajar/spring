package id.go.kemsos.simpkh.domain;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Domain {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Domain.class, args);
    }

}